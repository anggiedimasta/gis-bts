'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('index', {
		title: 'GIS BTS Kabupaten Sumba Timur'
	});
});

/* GET home page. */
router.get('/admin', function (req, res, next) {
	res.render('admin', {
		title: 'GIS BTS Kabupaten Sumba Timur - Admin'
	});
});

/* GET home page. */
router.get('/information', function (req, res, next) {
	res.render('information', {
		title: 'GIS BTS Kabupaten Sumba Timur - Information'
	});
});

/* GET home page. */
router.get('/map', function (req, res, next) {
	res.render('map', {
		title: 'GIS BTS Kabupaten Sumba Timur - Map'
	});
});

// catch 404 and forward to error handler
router.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
router.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = router;